import asyncio
from collections import defaultdict
import concurrent
import json
import logging
import random
import websockets

from bigbluebutton_api_python.core import ApiMethod
from bigbluebutton_api_python.util import UrlBuilder

logging.basicConfig()

class BotServer(object):
    bots = set()
    bot_names = {}
    bots_active = set()
    bots_listen = set()
    bots_microphone = set()
    bots_camera_active = set()
    bot_queues = {}
    users = set()
    user_queues = {}
    user_tasks = {}
    UNKNOWN=1
    BOT=2
    USER=3
    logfile = None
    measurements = []

    def __init__(self, apiurl, secret):
        self.apiurl = apiurl
        self.secret = secret

    def log_action(self, bot, action, time, status):
        if self.logfile is None:
            self.logfile = open('/tmp/benchmark.log', 'w')
        self.logfile.write(f"{bot}:{action}:{time}:{status}\n")
        self.logfile.flush()
        pass

    async def users_send_from_queue(self, user):
        q = self.user_queues[user]
        try:
            while True:
                msg = await q.get()
                await user.send(msg)
        except websockets.exceptions.ConnectionClosed as e:
            logging.debug("connection closed while sending to user")
        except concurrent.futures.CancelledError:
            logging.debug("connection cancelled")
        finally:
            await self.unregister(user)

    async def notify_users(self):
        message = json.dumps({'users': len(self.users), 'bots': len(self.bots)})
        print("notify users: %s" % message)
        if self.users:
            for user in self.users:
                q = self.user_queues[user]
                await q.put(message)

    async def send_to_user(self, user, message):
        try:
            await user.send(message)
        except websockets.exceptions.ConnectionClosed:
            self.unregister(user)

    async def send_to_bot(self, bot, message):
        try:
            await bot.send(message)
        except websockets.exceptions.ConnectionClosed:
            botname = getattr(bot, 'connection_name', 'unbekannt')
            print(f"Connection closed: {botname}. Last message: {message}")
            await self.unregister(bot)

    async def handle_unknown(self, websocket, data):
        client_type=self.UNKNOWN
        if data["action"] == "type":
            if data["type"] == "bot":
                client_type = self.BOT
                self.bots.add(websocket)
                self.bot_queues[websocket] = asyncio.Queue()
                setattr(websocket, 'connection_name', data["name"])
            elif data["type"] == "user":
                client_type = self.USER
                self.users.add(websocket)
                self.user_queues[websocket] = asyncio.Queue()
                setattr(websocket, 'connection_name', 'user connection')
                task = asyncio.ensure_future(self.users_send_from_queue(websocket))
                self.user_tasks[websocket] = task
            await self.notify_users()
        return client_type

    async def handle_bot(self, websocket, data):
        connection_name = websocket.connection_name
        print(f"received from {connection_name}: {data}")
        if 'message' in data:
            self.log_action(connection_name, data['action'], data['time'], data['status'])
        if 'browser_console' in data:
            with open(f"/tmp/browser_console.{connection_name}.log", "a") as f:
                f.write(json.dumps(data['browser_console']))
        if 'measurement' in data:
            self.measurements.append(data['measurement'])
        q = self.bot_queues[websocket]
        message = await q.get()
        print(f"from queue: {message}")
        msg_data = json.loads(message)
        wait_time = msg_data.get("wait_time", 0)
        print(f"{connection_name}: waiting {wait_time}")
        await asyncio.sleep(wait_time)
        if msg_data.get('action', None) != 'wait':
            print(f"{connection_name}: sending {message}")
            await websocket.send(message)
        else:
            print(f"{connection_name}: _NOT_ sending {message}")
            await websocket.send(message)



    async def handle_user(self, websocket, data):
        print("user: %s" % str(data))
        if data["action"] == "start_browser":
            await self.start_browsers(data)
        elif data["action"] == "join":
            await self.join_bots_to_meeting(data)
        elif data["action"] == "audio":
            await self.audio_bots_to_meeting(data)
        elif data["action"] == "teardown":
            await self.teardown_bots(data)
        elif data["action"] == "chat":
            await self.chat_bots(data)
        elif data["action"] == "start_camera":
            await self.start_camera(data)
        elif data["action"] == "stop_camera":
            await self.stop_camera(data)
        elif data["action"] == "quit":
            await self.quit_bots(data)
        elif data["action"] == "wait":
            await self.wait_bots(data)
        elif data["action"] == "open_url":
            await self.relay_data(data)
        elif data["action"] == "click":
            await self.relay_data(data)
        elif data["action"] == "click_link":
            await self.relay_data(data)
        elif data["action"] == "type_name":
            await self.type_name(data)
        elif data["action"] == "screenshot":
            await self.relay_data(data)
        elif data["action"] == "report_text":
            await self.relay_data(data)
        elif data["action"] == "print_measurements":
            self.print_measurements()
        pass

    def print_measurements(self):
        print(f"Aktuelle Messwerte: {self.measurements}")
        values_per_measurement = defaultdict(lambda : defaultdict(list))
        for m in self.measurements:
            values_per_measurement[m['measurement']][m['text']].append(m['name'])
        for m in sorted(values_per_measurement.keys()):
            print(f"{m}\n{'=' * len(m)}")
            for text in sorted(values_per_measurement[m].keys()):
                print(f"  {text}: {len(values_per_measurement[m][text])}")


    async def start_browsers(self, data):
        if self.bots:
            for bot in self.bots:
                q = self.bot_queues[bot]
                await(q.put(json.dumps({
                    'action': 'start_browser'
                })))

    async def audio_bots_to_meeting(self, data):
        microphone_percent = data.get('microphone_percent', 0.0)
        botlist = sorted(list(self.bots), key=lambda bot: (int(getattr(bot, 'connection_name').split('-')[-1]), getattr(bot, 'connection_name')))
        random.shuffle(botlist)
        numbots = len(botlist)
        wait_time = data.get('wait_time', 5)
        microphone_percent = data.get('microphone_percent', 0.0)
        for i, bot in enumerate(self.bots):
            q = self.bot_queues[bot]
            connection_name = getattr(bot, 'connection_name', f'Bot {i}')
            data = json.dumps({
                'action': {True: "audio_microphone", False: "audio_listen"}[i+1 <= (numbots * microphone_percent/100)],
                'wait_time': i * wait_time,
                'numbots': numbots,
            })
            print(f"Sending to {connection_name}: {data}")
            await q.put(data)


    async def join_bots_to_meeting(self, data):
        meetingID = data['meetingID']
        password = data['password']
        wait_time = data.get('wait_time', 5)
        microphone_percent = data.get('microphone_percent', None)
        botlist = sorted(list(self.bots), key=lambda bot: (int(getattr(bot, 'connection_name').split('-')[-1]), getattr(bot, 'connection_name')))
        #random.shuffle(botlist)
        numbots = len(botlist)
        for i, bot in enumerate(self.bots):
            q = self.bot_queues[bot]
            connection_name = getattr(bot, 'connection_name', f'Bot {i}')
            if microphone_percent:
                action = {True: "join_microphone", False: "join_listen"}[i+1 <= (numbots * microphone_percent/100)]
            else:
                action = 'join_only'
            data = json.dumps({
                'action': action,
                'url': self.join_url(meetingID, connection_name,  password),
                'wait_time': i * wait_time,
                'numbots': numbots,
            })
            print(f"Sending to {connection_name}: {data}")
            await q.put(data)

    async def type_name(self, data):
        for i, bot in enumerate(self.bots):
            q = self.bot_queues[bot]
            connection_name = getattr(bot, 'connection_name', f'Bot {i}')
            msg = json.dumps({
                'action': 'type_text',
                'text': connection_name,
                'locator_type': data['locator_type'],
                'locator_expression': data['locator_expression'],
            })
            print(f"Sending to {connection_name}: {msg}")
            await q.put(msg)

    async def wait_bots(self, data):
        if self.bots:
            for bot in self.bots:
                q = self.bot_queues[bot]
                await(q.put(json.dumps(data)))

    async def relay_data(self, data):
        if self.bots:
            delay_each = None
            if 'delay_each' in data:
                delay_each = data['delay_each']
            for i, bot in enumerate(self.bots):
                if delay_each:
                    data['wait_time'] = i * delay_each
                q = self.bot_queues[bot]

                await(q.put(json.dumps(data)))

    async def teardown_bots(self, data):
        if self.bots:
            for bot in self.bots:
                q = self.bot_queues[bot]
                await(q.put(json.dumps({
                    'action': 'teardown'
                })))

    async def quit_bots(self, data):
        if self.bots:
            max_quit = len(self.bots)
            if 'limit_bots' in data:
                max_quit = data['limit_bots']
            for i, bot in enumerate(self.bots):
                if i >= max_quit:
                    break
                q = self.bot_queues[bot]
                await(q.put(json.dumps({
                    'action': 'quit'
                })))
        else:
            print("keine Bots zum beenden")

    async def chat_bots(self, data):
        if self.bots:
            message = data.get("message", "foo bar baz")
            wait_time = data.get('wait_time', 5)
            for i, bot in enumerate(self.bots):
                q = self.bot_queues[bot]
                await(q.put(json.dumps({
                    'action': 'chat',
                    'message': message,
                    'wait_time': wait_time,
                })))

    async def start_camera(self, data):
        if self.bots:
            wait_time = data.get('wait_time', 5)
            for i, bot in enumerate(self.bots):
                q = self.bot_queues[bot]
                await(q.put(json.dumps({
                    'action': 'start_camera',
                    'wait_time': i * wait_time,
                })))
            self.bots_camera_active.update(self.bots)

    async def stop_camera(self, data):
        if self.bots_camera_active:
            wait_time = data.get('wait_time', 5)
            for i, bot in enumerate(self.bots):
                q = self.bot_queues[bot]
                await(q.put(json.dumps({
                    'action': 'stop_camera',
                    'wait_time': i * wait_time,
                })))
            self.bots_camera_active = set()

    def join_url(self, meetingID, name, password):
        urlbuilder = UrlBuilder(self.apiurl, self.secret)
        params = {
            'meetingID': meetingID,
            'fullName': name,
            'password': password,
            'joinViaHtml5': True,
        }
        return urlbuilder.buildUrl(ApiMethod.JOIN, params=params)

    def create_meeting(self, meetingID):
        urlbuilder = UrlBuilder(self.apiurl, self.secret)
        params = {
            'meetingID': meetingID,
            'attendeePW': 'ap',
            'moderatorPW': 'mp',
        }
        return urlbuilder.buildUrl(ApiMethod.JOIN, params=params)
        

    async def new_connection(self, websocket, path):
        client_type = self.UNKNOWN
        try:
            async for message in websocket:
                print("%s received %s" % (client_type, message))
                data = json.loads(message)
                if client_type == self.UNKNOWN:
                    client_type = await self.handle_unknown(websocket, data)
                elif client_type == self.BOT:
                    await self.handle_bot(websocket, data)
                elif client_type == self.USER:
                    await self.handle_user(websocket, data)
        except websockets.exceptions.ConnectionClosed as e:
            logging.debug("%s disconnected" % client_type)
        except:
            logging.exception("Something weird happened")
        finally:
            await self.unregister(websocket)

    async def unregister(self, websocket):
        if websocket in self.bots:
            botname = getattr(websocket, 'connection_name', 'unbekannt')
        else:
            botname = 'user'
        print(f"start unregistering {botname}")
        for s in (self.bots, 
                  self.users,
                  self.bots_active,
                  self.bots_listen,
                  self.bots_camera_active,
                 ):
            if websocket in s:
                s.remove(websocket)
        if websocket in self.bot_queues:
            self.bot_queues.pop(websocket)
        if websocket in self.user_queues:
            self.user_queues.pop(websocket)
        if websocket in self.user_tasks:
            task = self.user_tasks[websocket]
            task.cancel()
        await websocket.close()
        await self.notify_users()
        print(f"finished unregistering {botname}")

    async def register(self, websocket):
        self.connections.add(websocket)
        await self.notify_users()

with open("server.config.json", "rb") as f:
    config = json.loads(f.read())

bs = BotServer(config['api'], config['secret'])
start_server = websockets.serve(bs.new_connection, "", 6789)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
