# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

#!/usr/bin/env python

import asyncio
import json
import websockets

async def program(websocket, fut):
        await websocket.send(json.dumps({'action': 'start_browser', 'wait_time': 0}))
        await asyncio.sleep(20)
        await websocket.send(json.dumps({'action': 'join', 'meetingID': 'Benchmark', 'password': 'ap', 'microphone_percent': None, 'wait_time': 0.1}))
        print("done")
        fut.set_result(True)

async def run(uri, minbots):
    fut = None
    program_run = False
    async with websockets.connect(uri) as websocket:
        await websocket.send(json.dumps({'action': 'type', 'type': 'user'}))
        async for message in websocket:
            print("received: %s" % message)
            data = json.loads(message)
            if fut is None and not program_run and data['bots'] >= minbots:
                print("Start sending commands to bots")
                program_run = True
                loop = asyncio.get_running_loop()
                fut = loop.create_future()
                loop.create_task(program(websocket, fut))
            if fut:
                if fut.done():
                    print("future is done")
                    fut.result()
                    fut = None
            # if the benchmark has run and all bots are gone, we can quit
            if program_run and data['bots'] == 0:
                return

        response = await websocket.recv()
        print(response)

if __name__ == '__main__':
    with open("client.config.json", "rb") as f:
        config = json.loads(f.read())
        wait_for_bots = config.get('wait_for_bots', 1)
        asyncio.get_event_loop().run_until_complete(run(config['uri'], wait_for_bots))
