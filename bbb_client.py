from selenium import webdriver
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options  
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import logging
import time

class BbbClient(object):
    def __init__(self):
        self.browser = None
    
    def start_browser(self):
        """
        starts browser
        """
        if self.browser:
            return
        options = Options()
        options.add_argument('--headless')
        options.add_experimental_option("excludeSwitches", ['enable-automation'])
        options.add_argument('--shm-size=1gb')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--no-user-gesture-required')
        options.add_argument('--use-fake-ui-for-media-stream')
        options.add_argument('--use-fake-device-for-media-stream')
        options.add_argument('--lang=en_US,en')
        options.add_argument('--autoplay-policy=no-user-gesture-required')
        options.add_argument('--temp-profile')
        #options.binary_location = '/tmp/bbb-bench/bin/chromedriver'
        options.headless = True

        #set browser log
        dc = DesiredCapabilities.CHROME
        dc['goog:loggingPrefs'] = { 'browser':'ALL' }
        options.set_capability(name='goog:loggingPrefs', value={ 'browser':'ALL' })

        logging.info('Starting browser!!')
        #self.browser = webdriver.Chrome(executable_path='chromedriver', options=options, desired_capabilities=dc)
        self.browser = webdriver.Chrome(options=options)
        self.browser.set_window_size(1280, 1080)

    def stop_browser(self):
        """
        terminates browser
        """
        if self.browser:
            self.browser.quit()
            self.browser = None

    def screenshot(self, filename):
        if self.browser:
            self.browser.get_screenshot_as_file(filename)

    def _wait_for(self, locator, timeout=60):
        element = EC.visibility_of_element_located(locator)
        WebDriverWait(self.browser, timeout).until(element)

    def _wait_for_invisibility(self, locator, timeout=60):
        element = EC.invisibility_of_element_located(locator)
        WebDriverWait(self.browser, timeout).until(element)

    def _click(self, locator):
        self.browser.find_element(locator[0], locator[1]).click()

    def _wait_and_click(self, locator, timeout=60):
        self._wait_for(locator, timeout)
        self._click(locator)

    def type_text(self, locator, message):
        element = self.browser.find_element(locator[0], locator[1])
        element.send_keys(message)

    def wait_and_click_link(self, text, timeout=60):
        locator = (By.LINK_TEXT, text)
        self._wait_and_click(locator, timeout)

    def open_url(self, url):
        self.browser.get(url)

    def join_bbb(self, join_url):
        """
        opens bbb in browser
        """
        logging.info('Open BBB and hide elements!!')
        logging.info(join_url)
        self.browser.get(join_url)

        logging.info("browser got URL")

    def audio_mic(self):
        logging.info("waiting for overlay")
        self._wait_for((By.CSS_SELECTOR, '[data-test="audioModal"]'), 120)
        self._click((By.CSS_SELECTOR, '[data-test="microphoneBtn"]'))
        self._wait_and_click((By.CSS_SELECTOR, '[data-test="joinEchoTestButton"]'))
        self._wait_for_invisibility((By.CSS_SELECTOR, '[data-test="audioModal"]'))

    def audio_listen(self):
        logging.info("waiting for overlay")
        self._wait_for((By.CSS_SELECTOR, '[data-test="audioModal"]'), 120)
        self._click((By.CSS_SELECTOR, '[data-test="listenOnlyBtn"]'))
        logging.info("Waiting for overlay to disappear")
        self._wait_for_invisibility((By.CSS_SELECTOR, '[data-test="audioModal"]'))

    def mute(self):
        self._click((By.CSS_SELECTOR, '[data-test="muteMicButton"]'))

    def unmute(self):
        self._click((By.CSS_SELECTOR, '[data-test="unmuteMicButton"]'))

    def start_camera(self):
        self._wait_and_click((By.CSS_SELECTOR, '[data-test="joinVideo"]'))
        self._wait_and_click((By.CSS_SELECTOR, '[data-test="startSharingWebcam"]'))
        self._wait_for_invisibility((By.CSS_SELECTOR, '[data-test="webcamSettingsModal"]'))

    def stop_camera(self):
        self._wait_and_click((By.CSS_SELECTOR, '[data-test="leaveVideo"]'))

    def chat(self, message):
        self._wait_for((By.CSS_SELECTOR, '#message-input'))
        logging.info("start typing message")
        #time.sleep(20)
        #raise RuntimeError('foo')
        self.browser.find_element(By.CSS_SELECTOR, '#message-input').send_keys(message)
        self._click((By.CSS_SELECTOR, '[data-test="sendMessageButton"]'))

    def take_presenter(self):
        self._click((By.CSS_SELECTOR, '[aria-label="Actions"]'))
        self._wait_and_click((By.XPATH, "//span[text()='Take presenter']"))

    def raise_hand(self):
        self._click((By.CSS_SELECTOR, '[aria-label="Raise hand"]'))

    def console_log(self):
        return self.browser.get_log('browser')


if __name__ == '__main__':
    c = BbbClient()
    c.start_browser()
    join_url = "some_join_link_generated_from_api_mate_or_something_else"
    c.join_bbb(join_url)
    c.audio_mic()
    c.mute()
    c.start_camera()
    c.chat("Some noise")
    c.take_presenter()
    c.chat("More noise")
    time.sleep(30)
    c.stop_browser()
