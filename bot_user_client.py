#!/usr/bin/env python

import asyncio
import json
from multiprocessing import Process
from datetime import datetime
import time
import socket
import os
import sys
import traceback
try:
    import websockets
except ModuleNotFoundError:
    print("websocket not found: %s" % socket.getfqdn())
    sys.exit()
from selenium.webdriver.common.by import By

from bbb_client import BbbClient


def format_exception(e):
    exception_list = traceback.format_stack()
    exception_list = exception_list[:-2]
    exception_list.extend(traceback.format_tb(sys.exc_info()[2]))
    exception_list.extend(traceback.format_exception_only(sys.exc_info()[0], sys.exc_info()[1]))

    exception_str = "Traceback (most recent call last):\n"
    exception_str += "".join(exception_list)
    # Removing the last \n
    exception_str = exception_str[:-1]

    return exception_str

def message_handler(b, data, name):
    print(f"message_handler {data}")
    return_data = None
    if data['action'] == 'start_browser':
        b.start_browser()
    elif data['action'].startswith('join_'):
        b.join_bbb(data['url'])
        if data['action'] == 'join_microphone':
            b.audio_mic()
            b.mute()
        if data['action'] == 'join_listen':
            b.audio_listen()
    elif data['action'] == 'audio_microphone':
        b.audio_mic()
    elif data['action'] == 'audio_listen':
        b.audio_listen()
    elif data['action'] == 'teardown':
        for e in b.browser.get_log("browser"):
            print(e)
        b.stop_browser()
    elif data['action'] == 'chat':
        b.chat(data.get('message', 'message missing'))
    elif data['action'] == 'quit':
        b.stop_browser()
        return True, None
    elif data['action'] == 'start_camera':
        b.start_camera()
    elif data['action'] == 'stop_camera':
        b.stop_camera()
    elif data['action'] == 'open_url':
        b.open_url(data['url'])
    elif data['action'] == 'click_link':
        if 'text' in data:
            b.wait_and_click_link(data['text'])
        else:
            b._wait_and_click((By.CSS_SELECTOR, data['css']))
    elif data['action'] == 'type_text':
        locator_type = {
            'id': By.ID,
            'css': By.CSS_SELECTOR,
        }[data['locator_type']]
        b.type_text((locator_type, data['locator_expression']), data['text'])
    elif data['action'] == 'screenshot':
        b.screenshot(f"/tmp/{name}-screenshot-{data['name']}.png")
    elif data['action'] == 'report_text':
        locator_type = {
            'id': By.ID,
            'css': By.CSS_SELECTOR,
        }[data['locator_type']]
        text = b.browser.find_element(locator_type, data['locator_expression']).text
        return_data = {
            'measurement': data['measurement'], 
            'name': name,
            'text': text,
        }

    print("message_handler ende")
    return False, return_data

async def hello(uri, name):
    b = BbbClient()
    loop = asyncio.get_event_loop()
    try:
        async with websockets.connect(uri) as websocket:
            await websocket.send(json.dumps({'action': 'type', 'type': 'bot', 'name': name}))
            await websocket.send(json.dumps({'status': 'ok'}))
            async for message in websocket:
                data = json.loads(message)
                t1 = datetime.now()
                try:
                    quit, return_data = await loop.run_in_executor(None, message_handler, b, data, name)
                    t2 = datetime.now()
                    delta = (t2-t1).total_seconds()
                    send_data = {'status': 'ok', 'message': message, 'time': delta, 'action': data['action'] }
                    if return_data is not None:
                        send_data['measurement'] = return_data
                    await websocket.send(json.dumps(send_data))
                    if quit:
                        await websocket.close()
                        return
                except Exception as e:
                    print(e)
                    print(format_exception(e))
                    t2 = datetime.now()
                    delta = (t2-t1).total_seconds()
                    b.screenshot(f"/tmp/{name}-fail-{data['action']}.png")
                    logdata = b.console_log()
                    await websocket.send(json.dumps({'status': 'fail',
                                                     'message': message,
                                                     'exception': format_exception(e),
                                                     'time': delta,
                                                     'action': data['action'],
                                                     'browser_console': logdata,
                                                    }))
                    raise
    except Exception as e:
        print("Schlussexception")
        print(e)
    finally:
        b.stop_browser()

def run(uri, name):
    asyncio.get_event_loop().run_until_complete(hello(uri, name))

if __name__ == '__main__':
    with open("client.config.json", "rb") as f:
        config = json.loads(f.read())
    os.environ['LANG'] = 'en_US.UTF-8'
    instances = config['instances']
    with open('/proc/cpuinfo') as f:
        for line in f.readlines():
            if line.startswith('cpu cores'):
                cores = int(line.strip().split(':')[1].strip())
                instances = min(instances, int(cores*1.2))
                break
    with open('/proc/meminfo') as f:
        for line in f.readlines():
            if line.startswith('MemTotal:'):
                mem = int(line.split(':')[1].strip().split(' ')[0])
                mem = mem // 1024 // 1024
                instances = min(instances, mem-1)
                break
    if 'force_instances' in config:
        instances = config['force_instances']

    hostname = ".".join(socket.getfqdn().split('.')[0:2])
    for i in range(instances):
        p = Process(target=run, args=(config['uri'], f"{hostname}-{i}"))
        p.start()
        time.sleep(0.1)
